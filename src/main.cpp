#include <Arduino.h>
#include <webserver/Webserver.h>
#include "EasyOta.h"
#include <AccelStepper.h>

#define SSID "" //Enter SSID
#define PASSWORD "" //Enter Password
#define DIR_PIN    D4
#define STEP_PIN   D3
#define ENABLE_PIN D1
#define feedPortion 600


const unsigned long restart_interval = 2UL*60UL*60UL*1000UL;  
unsigned long previousMillis = 0; 

AccelStepper myStepper(1, STEP_PIN, DIR_PIN);
static WifiSetup ws;
static Webserver webserver(80);


void setup() {

  Serial.begin(9600);
  Serial.println("Starting up..");
  
 
  ws.setupWifi(SSID, PASSWORD);
  webserver.setup(myStepper, feedPortion); 

  pinMode(ENABLE_PIN, OUTPUT);
  digitalWrite(ENABLE_PIN, HIGH);
  
  myStepper.setMaxSpeed(1000);
	myStepper.setAcceleration(100);
	myStepper.setSpeed(600);

  EasyOta.setup();
  
}


void loop() {
  EasyOta.checkForUpload();
  if(myStepper.distanceToGo() != 0) {
    digitalWrite(ENABLE_PIN, LOW);
    myStepper.run();
  }
  else {
    myStepper.disableOutputs();
    digitalWrite(ENABLE_PIN, HIGH);
  }
   
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= restart_interval) {
    previousMillis = currentMillis;
    ESP.restart();
  }
  
}