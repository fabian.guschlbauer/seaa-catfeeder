#include <wifi/WifiSetup.h>

void WifiSetup::setupWifi(String ssid, String password) {

  // Connect to WiFi
  WiFi.disconnect();
  WiFi.begin(ssid, password);
  WiFi.mode(WIFI_AP_STA);
  while (WiFi.status() != WL_CONNECTED) {
    Serial.println("");
    delay(1000);
    Serial.println("Connecting ... ");
  }

  Serial.println("");
  Serial.println("WiFi connection Successful");
  Serial.print("The IP Address of ESP8266 Module is: ");
  // Print the IP address
  Serial.println(WiFi.localIP());
  
  
  WiFi.setAutoReconnect(reconnect);
  WiFi.persistent(true);
  
}

String WifiSetup::getIP() {
  return WiFi.softAPIP().toString();
}
