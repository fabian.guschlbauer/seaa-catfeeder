#include <ESP8266WiFi.h>

class WifiSetup {
  private:
    String ap_ssid = "catfeeder"; //Enter SSID
    String ap_password = "1234567890"; //Enter Password
  public:
    void setupWifi(String ssid, String password);
    boolean reconnect = true;
    String getIP();
};