#include <ESPAsyncWebServer.h>
#include <wifi/WifiSetup.h>
#include <AccelStepper.h>

class Webserver {
  protected:
    AsyncWebServer server;
  public:
    Webserver(int port);
    void setup(AccelStepper& stepper, int feedPortion);
};