#include <webserver/Webserver.h>
#include <LittleFS.h>
#include <ArduinoJson.h>

Webserver::Webserver(int port): server{port} {

};

void Webserver::setup(AccelStepper& stepper, int feedPortion) {
  
    server.on("/feed", HTTP_GET, [&](AsyncWebServerRequest *request) { 
      stepper.moveTo(stepper.currentPosition() + feedPortion);
      request->send(200);
      Serial.println("Request done");
    });

    server.onNotFound([&](AsyncWebServerRequest *request) { 
      Serial.print("Received request not found from client with IP: ");
      Serial.print(request->client()->remoteIP());
      Serial.print(" ");
      Serial.println(request->url());
      request->send(404, "text/html", "Not Found"); 
      Serial.println("Request done");
    });
  
    server.begin();
    Serial.println("");
    Serial.println("HTTP server started");
}
